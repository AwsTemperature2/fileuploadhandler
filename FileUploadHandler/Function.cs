using System;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.StepFunctions;
using Amazon.StepFunctions.Model;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace FileUploadHandler
{
    public class Function
    {
        IAmazonStepFunctions stepFunction { get; set; }

        /// <summary>
        /// Default constructor. This constructor is used by Lambda to construct the instance. When invoked in a Lambda environment
        /// the AWS credentials will come from the IAM role associated with the function and the AWS region will be set to the
        /// region the Lambda function is executed in.
        /// </summary>
        public Function()
        {
            stepFunction = new AmazonStepFunctionsClient(RegionEndpoint.EUCentral1);
        }

        /// <summary>
        /// Constructs an instance with a preconfigured S3 client. This can be used for testing the outside of the Lambda environment.
        /// </summary>
        /// <param name="s3Client"></param>
        public Function(IAmazonStepFunctions stepFunction)
        {
            this.stepFunction = stepFunction;
        }
        
        /// <summary>
        /// This method is called for every Lambda invocation. This method takes in an S3 event object and can be used 
        /// to respond to S3 notifications.
        /// </summary>
        /// <param name="evnt"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task FileUploadedHandler(S3Event evnt, ILambdaContext context)
        {
            var key = evnt.Records?[0].S3?.Object?.Key;
            if(key == null)
            {
                context.Logger.LogLine("Missing object key in incoming event");
                return;
            }

            context.Logger.LogLine($"Incoming file key : {key}");


            await CallStepFunction(key,
                             "arn:aws:states:eu-central-1:358786124023:stateMachine:ProcessIncomingFile-Dev",
                             $"Processing_{key}_{Guid.NewGuid().ToString()}".Replace("/","-")); // todo better
        }

        private async Task<string> CallStepFunction(string payload, string stepFunctionArn, string executionName)
        {
            var startExecutionRequest = new StartExecutionRequest()
            {
                Input = JsonConvert.SerializeObject(new { key = payload }),
                StateMachineArn = stepFunctionArn, // todo from config by environment
                Name = executionName
            };

            var response = await this.stepFunction.StartExecutionAsync(startExecutionRequest);
            Console.WriteLine($"Execution started with arn: {response.ExecutionArn}");
            return response.ExecutionArn;
        }
    }
}
