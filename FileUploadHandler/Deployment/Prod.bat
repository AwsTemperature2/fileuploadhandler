cd..
dotnet restore
dotnet lambda deploy-serverless file-upload-handler-prod --region eu-central-1 --profile default -tp "EnvironmentName=Production"
cd Deployment
pause
