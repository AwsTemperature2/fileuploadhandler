using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Amazon.Lambda.S3Events;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;
using Amazon.Lambda.TestUtilities;

namespace FileUploadHandler.Tests
{
    public class FunctionTest
    {
        [Fact]
        public async Task TestS3EventLambdaFunction()
        {
            //IAmazonS3 s3Client = new AmazonS3Client(RegionEndpoint.USWest2);

            var bucketName = "temperaturemm-upload";
            var key = "dev/jsoncmd-2018-03-07_12-02-01";
            
            var s3Event = new S3Event
            {
                Records = new List<S3EventNotification.S3EventNotificationRecord>
                {
                    new S3EventNotification.S3EventNotificationRecord
                    {
                        S3 = new S3EventNotification.S3Entity
                        {
                            Bucket = new S3EventNotification.S3BucketEntity {Name = bucketName },
                            Object = new S3EventNotification.S3ObjectEntity {Key = key }
                        }
                    }
                }
            };

            var context = new TestLambdaContext();

            // Invoke the lambda function and confirm the content type was returned.
            var function = new Function();
            await function.FileUploadedHandler(s3Event, context);
        }
    }
}
